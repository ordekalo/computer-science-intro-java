package HW1;

import java.util.Scanner;

public class Exc1 {
    /**
     * Author: Or Dekalo
     * ID: 313138141
     * Exercise: 1
     */
    public static void main(String[] args) {

        final float MARKETING_FACTOR = 0.15F;
        final float WRAPPING_FACTOR = 0.05F;
        final float PRODUCT_VALUE_FACTOR = 0.8F;
        int zero_counter = 0;

        int number1, number2, number3, number4, number5;
        double marketing, wrapping;
        double average, calculation;


        System.out.println("Please enter 3 to 5 manufacture prices\n" +
                "If a manufacture is missing enter 0");

        Scanner scanner = new Scanner(System.in);
        number1 = scanner.nextInt();
        if (number1 == 0) {
            zero_counter += 1;
        }

        number2 = scanner.nextInt();
        if (number2 == 0) {
            zero_counter += 1;
        }

        number3 = scanner.nextInt();
        if (number3 == 0) {
            zero_counter += 1;
        }

        number4 = scanner.nextInt();
        if (number4 == 0) {
            zero_counter += 1;
        }

        number5 = scanner.nextInt();
        if (number5 == 0) {
            zero_counter += 1;
        }

        if (zero_counter >= 3) {
            System.out.println("Error in input, You should enter minimum 3 prices");

        } else {
            System.out.println("Please enter marketing price and packaging price\n" +
                    "Enter 0 if it is free");
            int temp;
            for (int i = 0; i <= 4; i++) {
                if (number2 < number1) {
                    temp = number1;
                    number1 = number2;
                    number2 = temp;
                }

                if (number3 < number2) {
                    temp = number2;
                    number2 = number3;
                    number3 = temp;
                }

                if (number4 < number3) {
                    temp = number3;
                    number3 = number4;
                    number4 = temp;
                }

                if (number5 < number4) {
                    temp = number4;
                    number4 = number5;
                    number5 = temp;
                }
            }

            average = (double) (number4 + number3) / 2;

            System.out.println("Do you need marketing? Please provide the cost - 0=Default Price");
            marketing = scanner.nextInt();
            while (marketing < 0) {
                System.out.println("Please provide valid input");
                marketing = scanner.nextInt();
            }
            marketing = (marketing == 0) ? average * MARKETING_FACTOR : marketing * MARKETING_FACTOR;

            System.out.println("Do you need wrapping? Please provide the cost - 0=Default Price");
            wrapping = scanner.nextInt();
            while (wrapping < 0) {
                System.out.println("Please provide valid input");
                wrapping = scanner.nextInt();
            }
            wrapping = (wrapping == 0) ? average * WRAPPING_FACTOR : wrapping * WRAPPING_FACTOR;

            scanner.close();
            calculation = average * PRODUCT_VALUE_FACTOR + marketing + wrapping;
            System.out.printf("Total price is: %.1f \n", calculation);
        }

        System.out.println("Bye Bye");
    }
}
