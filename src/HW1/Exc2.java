package HW1;

import java.util.Scanner;

public class Exc2 {
    /**
     * Author: Or Dekalo
     * ID: 313138141
     * Exercise: 2
     */
    public static void main(String[] args) {
        int first, second, total = 0, counter = 0, reversed = 0;
        Scanner scanner;
        scanner = new Scanner(System.in);

        System.out.println("Enter 2 numbers with the same number if digits:");
        first = scanner.nextInt();
        second = scanner.nextInt();
        scanner.close();

        while (first > 0) {
            int temp_a = first % 10;
            first /= 10;

            int temp_b = second % 10;
            second /= 10;

            while (temp_a > 0) {
                if (counter >= 9) {
                    break;
                }
                total = temp_b + total * 10;
                temp_a -= 1;
                counter += 1;

            }

        }

        while (total > 0) {
            int temp = total % 10;
            reversed = temp + reversed * 10;
            total /= 10;
        }
        System.out.println("res = " + reversed);
    }
}
