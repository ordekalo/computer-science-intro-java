package HW1;

public class Exc4 {
    /**
     * Author: Or Dekalo
     * ID: 313138141
     * Exercise: 4
     */

    public static void main(String[] args) {
        int num = 1, sum, counter = 0, success_counter = 1;
        int cell1 = 0, cell2 = 0, cell3 = 0;
        while (counter < 3) {
            num += 1;
            sum = num;
            while (sum > 9) {
                int a, b;
                a = 0;
                for (int i = sum; i != 0; i = i / 10) {
                    b = i % 10;
                    a = a + b * b;
                }
                sum = a;
            }
            if (sum == 1) {
                System.out.println(success_counter + ") " + num + " is a happy number");
                success_counter += 1;
                counter += 1;
            } else {
                counter = 0;
            }
            if (counter == 1) {
                cell1 = num;
            } else if (counter == 2) {
                cell2 = num;
            } else if (counter == 3) {
                cell3 = num;
            }

        }
        System.out.printf("%d %d %d", cell1, cell2, cell3);
    }
}

