package HW1;

import java.util.Scanner;

public class Exc3 {
    /**
     * Author: Or Dekalo
     * ID: 313138141
     * Exercise: 3
     */
    public static void main(String[] args) {
        int input, largest = 0, smallest = 0, counter = 0;
        Scanner scanner;
        scanner = new Scanner(System.in);
        System.out.println("Please provide an input");
        input = scanner.nextInt();
        scanner.close();

        System.out.println("Input: " + input);

        int temp = input;
        while (temp > 0) {
            temp /= 10;
            counter += 1;
        }
        temp = input;
        for (int i = 0; i < counter - 1; i++) {
            int temp_a, temp_b;
            temp_a = temp % 10;
            temp /= 10;


            for (int j = 0; j < counter; j++) {
                temp_b = temp % 10;
                temp /= 10;

                if (temp_b > temp_a) {
                    int local = temp_a;
                    temp_a = temp_b;
                    temp_b = local;
                }

                largest = temp_a + largest * 10;
                temp_a = temp_b;
            }

            if (i < counter - 2) {
                int local = largest;
                largest = 0;

                for (int k = 0; k < counter; k++) {
                    int temp1 = local % 10;
                    local /= 10;
                    largest = temp1 + largest * 10;

                }
                temp = largest;
                largest = 0;
            }
        }

        System.out.println("The largest number is: " + largest);

        boolean flag = false;
        for (int m = 0; m < counter; m++) {
            int temp1 = largest % 10;
            if (temp1 == 0) {
                flag = true;
            }
            largest /= 10;
            if (m == 2 && flag) {
                smallest = temp1 + smallest * 100;
            } else {
                smallest = temp1 + smallest * 10;

            }
        }
        System.out.println("The smallest number is: " + smallest);

    }
}
