package Exams.E222017;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        System.out.println("What is your name?");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();

        ParkingLot parkingLot = new ParkingLot();
        System.out.printf("There are %d Reserved Places Currently\n", parkingLot.countReservedPlace());
    }
}
