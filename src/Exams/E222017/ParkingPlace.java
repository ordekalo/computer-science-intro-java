package Exams.E222017;

public class ParkingPlace {
    public enum Status {FREE, OCCUPIED, RESERVED}

    private Status status;
    private int row;
    private int column;
    private String ownerName;
    private static int places;

    public ParkingPlace(Status status, int row, int column) {
        this.status = status;
        this.row = row;
        this.column = column;
        places++;
    }

    Status getStatus() {
        return status;
    }

    String getOwnerName() {
        return ownerName;
    }

    void setStatus(Status status) {
        this.status = status;
    }

    void setRow(int row) {
        this.row = row;
    }

    void setColumn(int column) {
        this.column = column;
    }

    void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    int getRow() {
        return row;
    }

    int getColumn() {
        return column;
    }
}
