package Exams.E222017;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ParkingLot {
    private final int ROWS = 10;
    private final int COLS = 20;
    private ParkingPlace[][] parkingPlaces = new ParkingPlace[ROWS][COLS];

    public ParkingLot() {
        for (int i = 0; i < this.parkingPlaces.length; i++) {
            for (int j = 0; j < this.parkingPlaces[i].length; j++) {
                this.parkingPlaces[i][j] = new ParkingPlace(ParkingPlace.Status.FREE, i, j);
            }
        }
    }

    public int countReservedPlace() {
        int counter = 0;
        for (ParkingPlace[] parkingPlace : this.parkingPlaces) {
            for (ParkingPlace place : parkingPlace) {
                if (place.getStatus() == ParkingPlace.Status.RESERVED) {
                    counter++;
                }
            }
        }
        return counter;
    }

    public void displayCurrentParkingLotStatus() {
        for (ParkingPlace[] parkingPlace : this.parkingPlaces) {
            for (ParkingPlace place : parkingPlace) {
                switch (place.getStatus()) {
                    case FREE:
                        System.out.println(".");
                        break;
                    case OCCUPIED:
                        System.out.println(place.getOwnerName().charAt(0));
                    case RESERVED:
                        System.out.println("^");
                }
            }
        }
    }

    public void readFromFile(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        while (scanner.hasNext()) {
            int row = scanner.nextInt();
            int column = scanner.nextInt();
            ParkingPlace.Status status = ParkingPlace.Status.valueOf(scanner.next());
            String name = scanner.next();

            ParkingPlace place = this.parkingPlaces[row][column];
            place.setRow(row);
            place.setColumn(column);
            place.setOwnerName(name);
            place.setStatus(status);
        }
    }

    public void saveToFile(PrintWriter writer) {
        for (ParkingPlace[] parkingPlace : this.parkingPlaces) {
            for (ParkingPlace place : parkingPlace) {
                if (place.getStatus() != ParkingPlace.Status.FREE) {
                    writer.print(place.getRow() + " ");
                    writer.print(place.getColumn() + " ");
                    writer.print(place.getStatus() + " ");
                    writer.print(place.getOwnerName() + " ");
                }
            }
        }
    }

    public void parkIn(String userName, int row, int column) {
        ParkingPlace place = this.parkingPlaces[row][column];
        if (place.getOwnerName().equals(userName)) {
            place.setStatus(ParkingPlace.Status.OCCUPIED);
            return;
        }

        int counter = 0;

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (!isInBound(row + i, column + j)) {
                    counter++;
                } else if (this.parkingPlaces[row + i][column + j].getStatus() == ParkingPlace.Status.FREE) {
                    counter++;
                }
            }
        }

        if (counter == 9) {
            place.setStatus(ParkingPlace.Status.OCCUPIED);
        } else {
            System.out.println("Sorry, but you can't park here. Please Read the terms & conditions");
        }


    }

    public void parkOut(int row, int column) {
        this.parkingPlaces[row][column].setStatus(ParkingPlace.Status.FREE);
    }

    public boolean reserveParkng(String userName, int row, int column) {
        ParkingPlace place = this.parkingPlaces[row][column];

        int counter = 0;

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (!isInBound(row + i, column + j)) {
                    counter++;
                } else if (this.parkingPlaces[row + i][column + j].getStatus() == ParkingPlace.Status.FREE) {
                    counter++;
                }
            }
        }

        if (counter == 9) {
            place.setStatus(ParkingPlace.Status.RESERVED);
            place.setOwnerName(userName);
            return true;
        } else {
            System.out.println("Sorry, but you can't park here. Please Read the terms & conditions");
            return false;
        }
    }

    public boolean findPlace(int row) {
        int column = lookupRow(row, 0);
        if (column == -1) {
            return false;
        } else {
            System.out.println("Found Free Parking Space in Row:" + row + " Column:" + column);
            return true;
        }
    }

    private int lookupRow(int row, int column) {
        if (isInBound(row, column) && this.parkingPlaces[row][column].getStatus() == ParkingPlace.Status.FREE) {
            return column;
        } else if (!isInBound(row, column)) {
            return -1;
        } else {
            return lookupRow(row, column + 1);
        }
    }

    private boolean isInBound(int row, int column) {
        return row >= 0 && column >= 0 && row <= this.parkingPlaces.length && column <= this.parkingPlaces[0].length;
    }
}
