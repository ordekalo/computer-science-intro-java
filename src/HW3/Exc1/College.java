package HW3.Exc1;

import java.util.Arrays;

public class College {
    private String name;
    private int numOfLecturers;
    private Lecturer[] allLecturers;
    private int arrayPosition;


    public College(String name, int numOfLecturers, int maxLecturers) {
        this.setName(name);
        this.setNumOfLecturers(numOfLecturers);
        this.allLecturers = new Lecturer[maxLecturers];
    }

    public College(College college) {
        this.setName(college.name);
        this.setNumOfLecturers(college.numOfLecturers);

        Lecturer[]lecturers = college.getAllLecturers();
        this.setAllLecturers(Arrays.copyOf(lecturers, lecturers.length));

        this.arrayPosition = college.getArrayPosition();
    }

    public Lecturer[] getAllLecturers() {
        return allLecturers;
    }

    private void setAllLecturers(Lecturer[] allLecturers) {
        this.allLecturers = allLecturers;
    }

    public boolean addLecturer(Lecturer lecturer) {
        if (this.allLecturers.length - 1 > this.arrayPosition) {
            this.allLecturers[this.arrayPosition] = lecturer;
            this.arrayPosition++;
            return true;
        } else {
            return false;
        }
    }

    public void sortByNumOfTimesPenFalls() {
        Lecturer[] arr = this.allLecturers;
        int n = this.arrayPosition;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (arr[j].getNumOfTimesPenFalls() < arr[j + 1].getNumOfTimesPenFalls()) {
                    // swap arr[j+1] and arr[i]
                    Lecturer temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; i < arrayPosition; i++) {
            b.append(allLecturers[i]);
            if (i != arrayPosition - 1) {
                b.append(", ");
            }

        }
        b.append(']');

        return "\nCollege{" +
                "name='" + name + '\'' +
                ", numOfLecturers=" + numOfLecturers +
                ", allLecturers=" + b.toString() +
                '}';
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setNumOfLecturers(int numOfLecturers) {
        this.numOfLecturers = numOfLecturers;
    }

    private int getArrayPosition() {
        return arrayPosition;
    }


}
