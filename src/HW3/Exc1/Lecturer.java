package HW3.Exc1;

public class Lecturer {
    private String name, favoriteIceCream;
    private int numOfTimesPenFalls;
    private int id;

    private static int autoId = 1000;

    public Lecturer(String name, String favoriteIceCream, int numOfTimesPenFalls) {
        setName(name);
        setFavoriteIceCream(favoriteIceCream);
        setNumOfTimesPenFalls(numOfTimesPenFalls);
        setId();
    }

    private void setId() {
        this.id = autoId;
        autoId++;
    }

    @Override
    public String toString() {
        return "\nLecturer{" +
                "name='" + name + '\'' +
                ", favoriteIceCream='" + favoriteIceCream + '\'' +
                ", numOfTimesPenFalls=" + numOfTimesPenFalls +
                ", id=" + id +
                "}";
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setFavoriteIceCream(String favoriteIceCream) {
        this.favoriteIceCream = favoriteIceCream;
    }

    int getNumOfTimesPenFalls() {
        return numOfTimesPenFalls;
    }

    private void setNumOfTimesPenFalls(int numOfTimesPenFalls) {
        this.numOfTimesPenFalls = numOfTimesPenFalls;
    }
}
