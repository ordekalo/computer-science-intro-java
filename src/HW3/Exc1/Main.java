package HW3.Exc1;

public class Main {
    /**
     * Author: Or Dekalo
     * ID: 313138141
     * Exercise: 1
     */
    public static void main(String[] args) {
        College college = new College("Afeka College", 60, 80);
        Lecturer lecturer1 = new Lecturer("Victor", "Ben&Jerrys", 6);
        Lecturer lecturer2 = new Lecturer("alex", "Anita", 20);

        college.addLecturer(lecturer1);
        college.addLecturer(lecturer2);
        System.out.println(college);

        college.sortByNumOfTimesPenFalls();
        System.out.println(college);
    }
}
