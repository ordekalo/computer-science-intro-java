package HW3.Exc2;

public class Guest {
    private String name;
    private int passportNumber; //MUST BE POSITIVE

    public Guest(String name, int passportNumber) {
        this.name = name;
        setPassportNumber(passportNumber);
    }

    public int getPassportNumber() {
        return passportNumber;
    }

    private void setPassportNumber(int passportNumber) {
        if (passportNumber > 0) {
            this.passportNumber = passportNumber;
        } else {
            System.out.println("Illegal Password Number!");
        }
    }

    @Override
    public String toString() {
        return "\nGuest{" +
                "name='" + name + '\'' +
                ", passportNumber=" + passportNumber +
                '}';
    }
}
