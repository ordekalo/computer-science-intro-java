package HW3.Exc2;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Hotel {
    private int numOfUsedRooms;
    private Room[][] allRooms;
    private static LocalDateTime lastReservation = LocalDateTime.now();

    public Hotel(int floors, int roomsForFloor) {
        this.allRooms = new Room[floors][roomsForFloor];
        for (int i = 0; i < this.allRooms.length; i++) {
            for (int j = 0; j < this.allRooms[i].length; j++) {
                int numOfBeds = (int) (Math.random() * 4 + 1);
                Room room = new Room(numOfBeds);
                this.allRooms[i][j] = room;
            }
        }
    }

    public int addReservation(Guest[] guests) {
        int neededBeds = guests.length;
        for (int i = 0; i < this.allRooms.length; i++) {
            for (int j = 0; j < this.allRooms[i].length; j++) {
                if (this.allRooms[i][j].getNumOfBeds() >= neededBeds) {
                    this.allRooms[i][j].setAllGuests(guests);
                    lastReservation = LocalDateTime.now();
                    this.numOfUsedRooms++;
                    return (i + 1) * 100 + (j + 1);
                }
            }
        }
        return -1;
    }

    public int findGuest(int passportNumber) {
        for (int i = 0; i < this.allRooms.length; i++) {
            for (int j = 0; j < this.allRooms[i].length; j++) {
                Guest[] roomGuests = this.allRooms[i][j].getAllGuests();
                if (roomGuests.length > 0) {
                    for (Guest guest : roomGuests) {
                        if (guest.getPassportNumber() == passportNumber) {
                            return (i + 1) * 100 + (j + 1);
                        }
                    }
                }
            }
        }
        return -1;
    }

    public int findMostAvailableFloor() {
        int emptyCounter = 0, floorNumber = 0;
        for (int i = 0; i < this.allRooms.length; i++) {

            int tempCounter = 0;

            for (int j = 0; j < this.allRooms[i].length; j++) {
                Guest[] guests = this.allRooms[i][j].getAllGuests();
                if (guests == null) {
                    tempCounter++;
                }
            }

            if (tempCounter > emptyCounter) {
                emptyCounter = tempCounter;
                floorNumber = i;
            }
        }
        return floorNumber + 1;
    }

    public void printLastReservationTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        System.out.println(lastReservation.format(formatter));
    }

    public void presentHotelCapacity() {
        System.out.println("This is our Hotel capacity:");
        int counter = 0;
        for (int i = 0; i < this.allRooms.length; i++) {
            if (numOfUsedRooms > counter) {
                System.out.printf("In Floor Number %d:\n\n", (i + 1));
                for (int j = 0; j < this.allRooms[i].length; j++) {
                    Guest[] guests = this.allRooms[i][j].getAllGuests();
                    if (guests != null) {
                        System.out.printf("In Room Number %d Those Are The Guests:\n", (j + 1));
                        for (Guest guest : guests) {
                            if (guest != null) {
                                System.out.println(guest);
                                counter++;
                            }
                        }
                    }
                }
            } else {
                return;
            }


        }
    }
}


