package HW3.Exc2;

public class Room {
    private int numOfBeds; // VALUE BETWEEN 1-4
    private Guest[] allGuests;

    public Room(int numOfBeds) {
        this.numOfBeds = numOfBeds;
    }

    public int getNumOfBeds() {
        return numOfBeds;
    }

    public Guest[] getAllGuests() {
        return allGuests;
    }

    public void setAllGuests(Guest[] allGuests) {
        this.allGuests = allGuests;
    }
}
