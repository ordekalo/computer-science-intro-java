package HW4.Exc1;


import java.io.PrintWriter;

public class ContactsPage {
    private static int serial = 65; //A

    private char c;
    private Contact[] contacts;
    private int savedContactsCounter = 0;

    ContactsPage(int maxContactsPerPage) {
        this.c = (char) (serial++);
        this.contacts = new Contact[maxContactsPerPage];
    }

    char getLetter() {
        return c;
    }

    int getSavedContactsCounter() {
        return savedContactsCounter;
    }

    Contact[] getContacts() {
        return contacts;
    }

    boolean appendContact(Contact contact) {
        if (savedContactsCounter < contacts.length - 1) {
            contacts[savedContactsCounter] = contact;
            savedContactsCounter++;
            return true;
        } else {
            return false;
        }
    }

    Contact getContactByName(String contactName) {
        for (Contact contact : contacts) {
            if (contact != null && contact.getName().equals(contactName)) {
                return contact;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(c).append(" :\n");
        for (Contact contact : contacts) {
            if (contact != null)
                builder.append(contact.toString()).append('\n');
        }
        return builder.toString();
    }

    void savePage(PrintWriter writer) {
        for (Contact contact : contacts) {
            if (contact != null) {
                contact.save(writer);
            }
        }
    }
}

