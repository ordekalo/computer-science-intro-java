package HW4.Exc1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class PhoneBook {
    private final static int NUM_OF_LETTERS = 'Z' - 'A' + 1;

    private ContactsPage[] allPages;

    PhoneBook(int maxContactsPerPage) {
        this.allPages = new ContactsPage[NUM_OF_LETTERS];
        for (int i = 0; i < this.allPages.length; i++) {
            this.allPages[i] = new ContactsPage(maxContactsPerPage);
        }
    }

    PhoneBook(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        if (file.exists()) {
            this.allPages = new ContactsPage[NUM_OF_LETTERS];

            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                char letter = scanner.nextLine().charAt(0);
                int maxContactsPerPage = Integer.parseInt(scanner.nextLine());
                int savedContactsCounter = Integer.parseInt(scanner.nextLine());

                this.allPages[letter - 'A'] = new ContactsPage(maxContactsPerPage);
                for (int i = 0; i < savedContactsCounter; i++) {
                    Contact contact = new Contact(scanner);
                    this.allPages[letter - 'A'].appendContact(contact);
                }
            }
        }

    }

    Contact getContactByName(String contactName) {
        char upperFirstChar = Contact.getFirstLetterAsCapital(contactName);
        return this.allPages[upperFirstChar - 'A'].getContactByName(contactName);
    }

    Contact.PhoneNumberStatus addContact(String name, String phoneNumber) {
        Contact.PhoneNumberStatus status = Contact.checkIfPhoneNumberIsValid(phoneNumber);
        int position = 0;
        if (status == Contact.PhoneNumberStatus.OK)
            position = Contact.getFirstLetterAsCapital(name) - 'A';
        if (allPages[position].appendContact(new Contact(name, phoneNumber))) {
            return Contact.PhoneNumberStatus.OK;
        } else {
            return Contact.PhoneNumberStatus.FAIL;
        }
    }

    ContactsPage getFullestPage() {
        ContactsPage contactsPage = null;
        int fullestScore = 0;
        for (ContactsPage page : allPages) {
            int currentPageScore = page.getSavedContactsCounter();

            if (currentPageScore > fullestScore) {
                contactsPage = page;
                fullestScore = currentPageScore;
            }
        }
        return contactsPage;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("My Phone Book:\n");
        for (ContactsPage page : allPages) {
            if (page != null) {
                builder.append(page.toString()).append('\n');
            }
        }
        return builder.toString();
    }

    void save(String pathFile) throws FileNotFoundException {
        File file = new File(pathFile);
        PrintWriter writer = new PrintWriter(file);

        for (ContactsPage contactsPage : allPages) {
            if (contactsPage != null) {
                writer.println(contactsPage.getLetter());
                writer.println(contactsPage.getContacts().length);
                writer.println(contactsPage.getSavedContactsCounter());
                contactsPage.savePage(writer);
            }
        }
        writer.close();
    }

}
