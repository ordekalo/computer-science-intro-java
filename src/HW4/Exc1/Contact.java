package HW4.Exc1;

import java.io.PrintWriter;
import java.util.Scanner;

public class Contact {
    private String name;
    private String phoneNumber;

    public enum PhoneNumberStatus {OK, FAIL, PREFIX_INVALID_LEN, PREFIX_NOT_START_WITH_0, NUMBER_INVALID_LEN, NOT_ALL_DIGITS}

    Contact(Scanner scanner) {
        name = scanner.nextLine();
        phoneNumber = scanner.nextLine();
    }

    Contact(String name, String phoneNumber) {
        setName(name);
        setPhoneNumber(phoneNumber);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name + "  -->  " + phoneNumber;
    }

    static PhoneNumberStatus checkIfPhoneNumberIsValid(String phoneNumber) {
        //CHECK FIRST CHAR IS 0
        char firstChar = phoneNumber.charAt(0);
        if (firstChar != '0') {
            return PhoneNumberStatus.PREFIX_NOT_START_WITH_0;
        } else if (phoneNumber.indexOf('-') != 3) {
            return PhoneNumberStatus.PREFIX_INVALID_LEN;
        } else if (phoneNumber.length() != 11) {
            return PhoneNumberStatus.NUMBER_INVALID_LEN;
        } else {
            for (int i = 0; i < phoneNumber.length(); i++) {
                char c = phoneNumber.charAt(i);
                if (c == '-') {
                    continue;
                }
                if (!(48 <= c && c <= 57)) {
                    return PhoneNumberStatus.NOT_ALL_DIGITS;
                }
            }
        }
        return PhoneNumberStatus.OK;
    }

    static char getFirstLetterAsCapital(String name) {
        return (char) (name.charAt(0) & 0x5f);
    }

    PhoneNumberStatus setPhoneNumber(String phoneNumber) {
        PhoneNumberStatus status = checkIfPhoneNumberIsValid(phoneNumber);
        if (status == PhoneNumberStatus.OK)
            this.phoneNumber = phoneNumber;

        return status;
    }

    void save(PrintWriter writer) {
        writer.println(name);
        writer.println(phoneNumber);
    }


}