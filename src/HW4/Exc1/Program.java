package HW4.Exc1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Program {
    /**
     * Author: Or Dekalo
     * ID: 313138141
     * Exercise: 1
     */
    private final static String PHONE_BOOK_FILE_NAME = "src/HW4/Exc1/PhoneBook.txt";

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);

        PhoneBook phoneBook;

        File file = new File(PHONE_BOOK_FILE_NAME);
        if (file.exists()) {
            System.out.println("PhoneBook is Created Form File");
            phoneBook = new PhoneBook(PHONE_BOOK_FILE_NAME);

            System.out.println("PhoneBook Details:");
            System.out.println(phoneBook);
        } else {
            System.out.println("PhoneBook is Created Manually");
            phoneBook = new PhoneBook(10);
        }

        boolean fContinue = true;

        do {
            System.out.println("\nEnter Contact? y\\Y=YES n\\N=NO");
            char answer = scanner.next().charAt(0);
            if (answer == 'n' || answer == 'N')
                fContinue = false;
            else if (answer == 'y' || answer == 'Y') {
                scanner.nextLine(); // clean buffer
                System.out.println("Enter Contact Name: ");
                String name = scanner.nextLine();

                System.out.println("Enter Phone Number: ");
                String phoneNumber = scanner.nextLine();

                Contact.PhoneNumberStatus status = phoneBook.addContact(name, phoneNumber);
                if (status == Contact.PhoneNumberStatus.OK)
                    System.out.println("Contact is Added Successfully");
                else
                    System.out.println("Failed Adding The Contact, Phone is Invalid: " + status.name());
            } else
                System.out.println("Invalid Answer");
        } while (fContinue);

        System.out.println("PhoneBook Details:");
        System.out.println(phoneBook);

        System.out.println("\nEnter the Name Of The Contact You Want To Edit His Phone Number");
        scanner.nextLine(); // clean the buffer
        String name = scanner.nextLine();
        Contact contact = phoneBook.getContactByName(name);
        if (contact == null)
            System.out.println("No Such Contact");
        else {
            System.out.println("Enter New Phone Number: ");
            String number = scanner.nextLine();
            Contact.PhoneNumberStatus status = contact.setPhoneNumber(number);
            if (status != Contact.PhoneNumberStatus.OK)
                System.out.println("Failed Changing The Number: " + status.name());
        }

        System.out.println("\nPhoneBook details again:");
        System.out.println(phoneBook.toString());

        System.out.println("The Most Full page Is The Letter " + phoneBook.getFullestPage().getLetter());

        phoneBook.save(PHONE_BOOK_FILE_NAME);
    }

}
