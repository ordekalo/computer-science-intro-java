package HW4.Exc2;

import java.util.Arrays;

public class NumOfDifferentValues {
    /**
     * Author: Or Dekalo
     * ID: 313138141
     * Exercise: 2
     */
    private static int numOfDifferentValues(int[] arr, int size) {
        if (size != 0) {
            for (int i = 0; i < size - 1; i++) {
                if (arr[i] == arr[size - 1]) {
                    return numOfDifferentValues(arr, size - 1);
                }
            }
            return 1 + numOfDifferentValues(arr, size - 1);
        } else {
            return 0;
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[5];

        for (int j = 0; j < 5; j++) {
            for (int i = 0; i < arr.length; i++)
                arr[i] = (int) (Math.random() * 10);

            System.out.println(Arrays.toString(arr) + " --> " + numOfDifferentValues(arr, arr.length));
        }

    }

}
