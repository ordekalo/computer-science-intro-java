package HW4.Exc3;

public class IsDiagonalHasLettersSequence {
    /**
     * Author: Or Dekalo
     * ID: 313138141
     * Exercise: 3
     */
    private static boolean isDiagonalLettersSequence(char[][] mat, int rows) {
        if (rows == 0) {
            return true;
        } else if (mat[rows - 1][rows - 1] == (char) (97 + rows - 1))
            return isDiagonalLettersSequence(mat, rows - 1);
        else {
            return false;
        }
    }

    public static void main(String[] args) {
        char[][] mat = new char[5][5];

        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                mat[i][j] = (char) (int) ((Math.random() * 26) + 'a');
            }
        }

        for (int i = 0; i < mat.length; i++) {
            mat[i][i] = (char) ('a' + i);
        }
        // FAIL TEST
//        mat[2][2] = 'b';

        for (char[] chars : mat) {
            for (char aChar : chars) System.out.print(aChar + " ");
            System.out.println();
        }
        System.out.println(isDiagonalLettersSequence(mat, mat.length));

    }

}
