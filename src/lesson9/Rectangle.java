
public class Rectangle {
	private int width, height;

	public Rectangle() {
		// TODO Auto-generated constructor stub
		this(10, 10);
	}

	public Rectangle(int width, int height) {
		setHeight(height);
		setWidth(width);
	}

	public int getScope() {
		return 2 * width + 2 * height;
	}

	public int getArea() {
		return width * height;
	}

	public void show() {
		show('*');

	}

	public void show(char c) {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {

				if (i == 0 || i == height - 1 || j == 0 || j == width - 1) {
					System.out.print(c);
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}

	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		if (width > 0) {
			this.width = width;
		} else {
			this.width = 1;
		}
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		if (height > 0) {
			this.height = height;
		} else {
			this.height = 1;
		}
	}

	public static void main(String[] args) {
		Rectangle rectangle = new Rectangle();
		rectangle.show();
		rectangle.show('z');
		System.out.println("Scope is " + rectangle.getScope());
		System.out.println("Area is " + rectangle.getArea());

	}
}
