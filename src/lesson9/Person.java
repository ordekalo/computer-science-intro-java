
public class Person {
	private String name;
	private Person partner;

	public Person(String name) {
		// TODO Auto-generated constructor stub
		this(name, null);
	}

	public Person(String name, Person partner) {
		// TODO Auto-generated constructor stub
		setName(name);
		setPartner(partner);
		if (partner != null) {
			partner.setPartner(this);
		}

	}

	public void show() {
		System.out.print("My name is " + name);
		if (partner != null) {
			System.out.print(" and my parner name is " + partner.getName());
		}
		System.out.println();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Person getPartner() {
		return partner;
	}

	public void setPartner(Person partner) {
		this.partner = partner;
	}

	public static void main(String[] args) {
		Person person = new Person("Person");
		person.show();

		Person partner = new Person("GirlFriend", person);
		partner.show();

		person.show();
	}
}
