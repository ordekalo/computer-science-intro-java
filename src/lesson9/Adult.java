
public class Adult {
	private String name;
	private int id;
	private String job;

	public Adult(String name, int id) {
		// TODO Auto-generated constructor stub
		setId(id);
		setName(name);
	}

	public Adult(String name, int id, String job) {
		// TODO Auto-generated constructor stub
		this(name, id);
		setJob(job);

	}

	public Adult(Adult adult) {
		// TODO Auto-generated constructor stub
		setId(adult.id);
		setJob(adult.job);
		setName(adult.name);
	}

	public void show() {
		System.out.println("Name: " + getName());
		System.out.println("ID: " + getId());
		if (getJob() != null) {
			System.out.println("Job: " + getJob());
		}
		System.out.println();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static void main(String[] args) {
		Adult a1 = new Adult("Or", 370, "Engineer");
		Adult a2 = new Adult("Friend", 111);
		Adult a3 = new Adult(a1);
		a1.show();
		a2.show();
		a3.show();
	}
}
