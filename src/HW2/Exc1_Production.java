package HW2;

import java.util.Scanner;

public class Exc1_Production {
    /**
     * Author: Or Dekalo
     * ID: 313138141
     * Exercise: 1
     * MINE MAZE GAME
     */
    public static void main(String[] args) {
        //JUST EXECUTE AND ENJOY:)
        playMaze();


    }

    private static int inputLifeBank(Scanner scanner) {
        int life_bank;
        do {
            System.out.println("Please Enter An Amount Of Life Bank Between 1 to 100:");
            life_bank = scanner.nextInt();
        }
        while (life_bank < 0 || life_bank > 100);

        return life_bank;
    }

    private static int inputNumberOfColumns(Scanner scanner) {
        // GET INPUT OF COLUMN FROM END USER
        int numberOfColumns;
        do {
            System.out.println("Please Enter A Number Of Columns Between 5 to 15:");
            numberOfColumns = scanner.nextInt();
        }
        while ((numberOfColumns < 0) || (numberOfColumns > 15));

        return numberOfColumns;
    }

    private static int inputNumberOfRows(Scanner scanner) {
        // GET INPUT OF ROWS FROM END USER
        int numberOfRows;
        do {
            System.out.println("Please Enter A Number Of Rows Between 5 to 15:");
            numberOfRows = scanner.nextInt();
        }
        while ((numberOfRows < 0) || (numberOfRows > 15));

        return numberOfRows;
    }

    private static char[][] createPathMatrix(Scanner scanner, int rows, int columns) {
        // FINAL VARIABLES
        final char DOWN_SIGN = 'V';
        final char UP_SIGN = '^';
        final char LEFT_SIGN = '<';
        final char RIGHT_SIGN = '>';
        final char JUNCTION_SIGN = '+';
        final char GRASS_SIGN = 'O';
        final char MINE_SIGN = '*';

        //CREATE BLANK MATRIX
        char[][] pathMatrix = new char[rows][columns];

        //SCANNED INPUT
        char input;

        //FLAG KEY FOR VALID INPUT
        boolean valid;

        for (int row = 0; row < pathMatrix.length; row++) {
            for (int column = 0; column < pathMatrix[row].length; column++) {
                do {
                    //FLAG KEY FOR VALID INPUT
                    valid = false;

                    System.out.printf("Please Provide A Valid Char For Cell (%d,%d):", column, row);
                    input = scanner.next().charAt(0);
                    //CHECK VALID INPUT BEFORE ASSIGNING
                    if (input == UP_SIGN || input == DOWN_SIGN || input == RIGHT_SIGN || input == LEFT_SIGN || input == GRASS_SIGN
                            || input == MINE_SIGN || input == JUNCTION_SIGN) {
                        valid = true;
                    }
                    pathMatrix[row][column] = input;
                }
                while (!valid);
            }
        }

        return pathMatrix;
    }

    private static char[][] findTraceMatrix(char[][] pathMatrix, int entranceRow, int entranceColumn) {
        //CREATE A COPY OF ORIGINAL MATRIX - DEFECTING PREVENTION
        char[][] matrix = copyMatrix(pathMatrix);

        // FINAL VARIABLES
        final char DOWN_SIGN = 'V';
        final char UP_SIGN = '^';
        final char LEFT_SIGN = '<';
        final char RIGHT_SIGN = '>';
        final char JUNCTION_SIGN = '+';
        final char BEEN_HERE_SIGN = '#';
        char GRASS_SIGN = 'O';
        char MINE_SIGN = '*';
        final int NUMBER_OF_COLUMNS = matrix[0].length;
        final int NUMBER_OF_ROWS = matrix.length;

        //TO KNOW WHEN TO BREAK THE WHILE LOOP
        boolean exitFlag = false;
        char currentCell;

        do {
            currentCell = matrix[entranceRow][entranceColumn];
            if (isWalkable(currentCell)) {
                matrix[entranceRow][entranceColumn] = BEEN_HERE_SIGN;
            }

            switch (currentCell) {
                case DOWN_SIGN:
                    if (entranceRow == NUMBER_OF_ROWS - 1) {
                        exitFlag = true;
                        break;
                    }
                    entranceRow++;
                    break;

                case UP_SIGN:
                    if (entranceRow == 0) {
                        exitFlag = true;
                        break;
                    }
                    entranceRow--;
                    break;

                case LEFT_SIGN:
                    if (entranceColumn == 0) {
                        exitFlag = true;
                        break;
                    }
                    entranceColumn--;
                    break;

                case RIGHT_SIGN:
                    if (entranceColumn == NUMBER_OF_COLUMNS - 1) {
                        exitFlag = true;
                        break;
                    }
                    entranceColumn++;
                    break;

                case JUNCTION_SIGN: //CLOCKWISE
                    char cellValue;

                    //CHECK RIGHT
                    if ((entranceColumn != NUMBER_OF_COLUMNS - 1)) {
                        cellValue = matrix[entranceRow][entranceColumn + 1];
                        if ((cellValue != GRASS_SIGN) && (cellValue != MINE_SIGN) && (cellValue != BEEN_HERE_SIGN)) {
                            entranceColumn++;
                            break;
                        }
                    } else {
                        exitFlag = true;
                        break;
                    }
                    //CHECK DOWN
                    if (entranceRow != NUMBER_OF_ROWS - 1) {
                        cellValue = matrix[entranceRow + 1][entranceColumn];
                        if ((cellValue != GRASS_SIGN) && (cellValue != MINE_SIGN) && (cellValue != BEEN_HERE_SIGN)) {
                            entranceRow++;
                            break;
                        }
                    } else {
                        exitFlag = true;
                        break;
                    }
                    //CHECK LEFT
                    if (entranceColumn != 0) {
                        cellValue = matrix[entranceRow][entranceColumn - 1];
                        if ((cellValue != GRASS_SIGN) && (cellValue != MINE_SIGN) && (cellValue != BEEN_HERE_SIGN)) {
                            entranceColumn--;
                            break;
                        }
                    } else {
                        exitFlag = true;
                        break;
                    }
                    //CHECK UP
                    if (entranceRow != 0) {
                        cellValue = matrix[entranceRow - 1][entranceColumn];
                        if ((cellValue != GRASS_SIGN) && (cellValue != MINE_SIGN) && (cellValue != BEEN_HERE_SIGN)) {
                            entranceRow--;
                            break;
                        }
                    } else {
                        exitFlag = true;
                        break;
                    }

                    break;
                case BEEN_HERE_SIGN:
                    //ANTI SELF LOOPING VALIDATION
                    return pathMatrix;

                default:
                    exitFlag = true;
                    break;
            }
        }
        while (!exitFlag);

        return matrix;
    }

    private static void playMaze() {
        //CREATE SCANNER OBJECT FOR USER INPUTS
        Scanner scanner = new Scanner(System.in);

        //CREATE THE PATH MATRIX BY USER INPUT
        char[][] pathMatrix = createPathMatrix(scanner, inputNumberOfRows(scanner), inputNumberOfColumns(scanner));

        //CREATE A COPY OF ORIGINAL MATRIX - DEFECTING PREVENTION
        char[][] matrix = copyMatrix(pathMatrix);

        // FINAL VARIABLES
        final char DOWN_SIGN = 'V';
        final char UP_SIGN = '^';
        final char LEFT_SIGN = '<';
        final char RIGHT_SIGN = '>';
        final char JUNCTION_SIGN = '+';
        final char BEEN_HERE_SIGN = '#';
        char MINE_SIGN = '*';
        final int NUMBER_OF_COLUMNS = matrix[0].length;
        final int NUMBER_OF_ROWS = matrix.length;

        //USER INPUT LIFE BANK
        int lifeBank = inputLifeBank(scanner);

        //OBTAIN MAZE ENTRANCE COORDINATES AS AN ARRAY
        int[] mazeEntrance = getMazeEntrance(pathMatrix);

        //ASSIGNING ENTRANCE TO VARIABLES
        int entranceRow = mazeEntrance[0];
        int entranceColumn = mazeEntrance[1];

        //ADD START COORDINATE TO ROUTE
        String routeDesc = "(" + entranceRow + "," + entranceColumn + ") -> ";

        //GAME STARTUP MESSAGE
        System.out.println("Starting at (" + entranceRow + "," + entranceColumn + ") with " + lifeBank + " lives...");

        //TO KNOW WHEN TO BREAK THE WHILE LOOP
        boolean exitFlag = false;
        char currentCell;

        do {
            currentCell = matrix[entranceRow][entranceColumn];
            if (isWalkable(currentCell)) {
                matrix[entranceRow][entranceColumn] = BEEN_HERE_SIGN;
                routeDesc = routeDesc.concat("(" + entranceRow + "," + entranceColumn + ") -> ");
            }

            switch (currentCell) {
                case DOWN_SIGN:
                    if (entranceRow == NUMBER_OF_ROWS - 1) {
                        System.out.println("Thanks god, we are out, good job…");
                        exitFlag = true;
                        break;
                    }
                    System.out.println("Moving down");
                    entranceRow++;
                    break;

                case UP_SIGN:
                    if (entranceRow == 0) {
                        System.out.println("Thanks god, we are out, good job…");
                        exitFlag = true;
                        break;
                    }
                    System.out.println("Moving up");
                    entranceRow--;
                    break;

                case LEFT_SIGN:
                    if (entranceColumn == 0) {
                        exitFlag = true;
                        System.out.println("Thanks god, we are out, good job…");
                        break;
                    }
                    System.out.println("Moving left");
                    entranceColumn--;
                    break;

                case RIGHT_SIGN:
                    if (entranceColumn == NUMBER_OF_COLUMNS - 1) {
                        System.out.println("Thanks god, we are out, good job…");
                        exitFlag = true;
                        break;
                    }
                    System.out.println("Moving right");
                    entranceColumn++;
                    break;

                case JUNCTION_SIGN: //CLOCKWISE
                    char cellValue;

                    System.out.println("I have reached a junction");
                    System.out.println("I'm going to check the area...");

                    //CHECK RIGHT
                    if ((entranceColumn != NUMBER_OF_COLUMNS - 1)) {
                        System.out.printf("Looking to (%d,%d)\n", entranceRow, entranceColumn + 1);
                        cellValue = matrix[entranceRow][entranceColumn + 1];
                        if (isWalkable(cellValue)) {
                            System.out.println("CLEAR!!!");
                            System.out.println("Let's go...");
                            entranceColumn++;

                            break;
                        } else if (cellValue == MINE_SIGN) {
                            if (lifeBank == 0) {
                                char decision = inputDecision(scanner);
                                switch (decision) {
                                    case 'n':
                                        System.out.println("You lose because you are a CHICKEN!");
                                        exitFlag = true;
                                        break;

                                    case DOWN_SIGN:
                                        if (entranceRow == NUMBER_OF_ROWS - 1) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow + 1][entranceColumn] != MINE_SIGN) {
                                            System.out.println("Moving down");
                                            entranceRow++;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case UP_SIGN:
                                        if (entranceRow == 0) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow - 1][entranceColumn] != MINE_SIGN) {
                                            System.out.println("Moving up");
                                            entranceRow--;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case LEFT_SIGN:
                                        if (entranceColumn == 0) {
                                            exitFlag = true;
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            break;
                                        } else if (matrix[entranceRow][entranceColumn - 1] != MINE_SIGN) {
                                            System.out.println("Moving left");
                                            entranceColumn--;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case RIGHT_SIGN:
                                        if (entranceColumn == NUMBER_OF_COLUMNS - 1) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow][entranceColumn + 1] != MINE_SIGN) {
                                            System.out.println("Moving right");
                                            entranceColumn++;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            lifeBank--;
                                            exitFlag = true;
                                            break;
                                        }
                                }

                                break;
                            } else {
                                System.out.println("BOOOOOM!!!");
                                lifeBank--;
                                System.out.printf("One life is gone, %d more left...\n", lifeBank);
                            }
                        }
                    } else {
                        exitFlag = true;
                        System.out.println("You Fall Off The Maze. Game Over!");
                        break;
                    }
                    //CHECK DOWN
                    if (entranceRow != NUMBER_OF_ROWS - 1) {
                        System.out.printf("Looking to (%d,%d)\n", entranceRow + 1, entranceColumn);
                        cellValue = matrix[entranceRow + 1][entranceColumn];
                        if (isWalkable(cellValue)) {
                            System.out.println("CLEAR!!!");
                            System.out.println("Let's go...");
                            entranceRow++;

                            break;

                        } else if (cellValue == MINE_SIGN) {
                            if (lifeBank == 0) {
                                char decision = inputDecision(scanner);
                                switch (decision) {
                                    case 'n':
                                        System.out.println("You lose because you are a CHIKEN!");
                                        exitFlag = true;
                                        break;

                                    case DOWN_SIGN:
                                        if (entranceRow == NUMBER_OF_ROWS - 1) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow + 1][entranceColumn] != MINE_SIGN) {
                                            System.out.println("Moving down");
                                            entranceRow++;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case UP_SIGN:
                                        if (entranceRow == 0) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow - 1][entranceColumn] != MINE_SIGN) {
                                            System.out.println("Moving up");
                                            entranceRow--;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case LEFT_SIGN:
                                        if (entranceColumn == 0) {
                                            exitFlag = true;
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            break;
                                        } else if (matrix[entranceRow][entranceColumn - 1] != MINE_SIGN) {
                                            System.out.println("Moving left");
                                            entranceColumn--;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case RIGHT_SIGN:
                                        if (entranceColumn == NUMBER_OF_COLUMNS - 1) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow][entranceColumn + 1] != MINE_SIGN) {
                                            System.out.println("Moving right");
                                            entranceColumn++;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            lifeBank--;
                                            exitFlag = true;
                                            break;
                                        }
                                }

                                break;
                            } else {
                                System.out.println("BOOOOOM!!!");
                                lifeBank--;
                                System.out.printf("One life is gone, %d more left...\n", lifeBank);
                            }
                        }
                    } else {
                        exitFlag = true;
                        System.out.println("You Fall Off The Maze. Game Over!");
                        break;
                    }
                    //CHECK LEFT
                    if (entranceColumn != 0) {
                        System.out.printf("Looking to (%d,%d)\n", entranceRow, entranceColumn - 1);
                        cellValue = matrix[entranceRow][entranceColumn - 1];
                        if (isWalkable(cellValue)) {
                            System.out.println("CLEAR!!!");
                            System.out.println("Let's go...");
                            entranceColumn--;

                            break;
                        } else if (cellValue == MINE_SIGN) {
                            if (lifeBank == 0) {
                                char decision = inputDecision(scanner);
                                switch (decision) {
                                    case 'n':
                                        System.out.println("You lose because you are a CHIKEN!");
                                        exitFlag = true;
                                        break;

                                    case DOWN_SIGN:
                                        if (entranceRow == NUMBER_OF_ROWS - 1) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow + 1][entranceColumn] != MINE_SIGN) {
                                            System.out.println("Moving down");
                                            entranceRow++;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case UP_SIGN:
                                        if (entranceRow == 0) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow - 1][entranceColumn] != MINE_SIGN) {
                                            System.out.println("Moving up");
                                            entranceRow--;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case LEFT_SIGN:
                                        if (entranceColumn == 0) {
                                            exitFlag = true;
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            break;
                                        } else if (matrix[entranceRow][entranceColumn - 1] != MINE_SIGN) {
                                            System.out.println("Moving left");
                                            entranceColumn--;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case RIGHT_SIGN:
                                        if (entranceColumn == NUMBER_OF_COLUMNS - 1) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow][entranceColumn + 1] != MINE_SIGN) {
                                            System.out.println("Moving right");
                                            entranceColumn++;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            lifeBank--;
                                            exitFlag = true;
                                            break;
                                        }
                                }

                                break;
                            } else {
                                System.out.println("BOOOOOM!!!");
                                lifeBank--;
                                System.out.printf("One life is gone, %d more left...\n", lifeBank);
                            }

                        }
                    } else {
                        exitFlag = true;
                        System.out.println("You Fall Off The Maze. Game Over!");
                        break;
                    }
                    //CHECK UP
                    if (entranceRow != 0) {
                        System.out.printf("Looking to (%d,%d)\n", entranceRow - 1, entranceColumn);
                        cellValue = matrix[entranceRow - 1][entranceColumn];
                        if (isWalkable(cellValue)) {
                            System.out.println("CLEAR!!!");
                            System.out.println("Let's go...");
                            entranceRow--;

                            break;

                        } else if (cellValue == MINE_SIGN) {
                            if (lifeBank == 0) {
                                char decision = inputDecision(scanner);
                                switch (decision) {
                                    case 'n':
                                        System.out.println("You lose because you are a CHIKEN!");
                                        exitFlag = true;
                                        break;

                                    case DOWN_SIGN:
                                        if (entranceRow == NUMBER_OF_ROWS - 1) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow + 1][entranceColumn] != MINE_SIGN) {
                                            System.out.println("Moving down");
                                            entranceRow++;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case UP_SIGN:
                                        if (entranceRow == 0) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow - 1][entranceColumn] != MINE_SIGN) {
                                            System.out.println("Moving up");
                                            entranceRow--;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case LEFT_SIGN:
                                        if (entranceColumn == 0) {
                                            exitFlag = true;
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            break;
                                        } else if (matrix[entranceRow][entranceColumn - 1] != MINE_SIGN) {
                                            System.out.println("Moving left");
                                            entranceColumn--;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            exitFlag = true;
                                            lifeBank--;
                                            break;
                                        }

                                    case RIGHT_SIGN:
                                        if (entranceColumn == NUMBER_OF_COLUMNS - 1) {
                                            System.out.println("You Fall Off The Maze. Game Over!");
                                            exitFlag = true;
                                            break;
                                        } else if (matrix[entranceRow][entranceColumn + 1] != MINE_SIGN) {
                                            System.out.println("Moving right");
                                            entranceColumn++;
                                            break;
                                        } else {
                                            System.out.println("BOOOM!!! Game Over");
                                            lifeBank = 0;
                                            exitFlag = true;
                                            break;
                                        }
                                }

                                break;
                            } else {
                                System.out.println("BOOOOOM!!!");
                                lifeBank--;
                                System.out.printf("One life is gone, %d more left...\n", lifeBank);
                            }
                        }
                    } else {
                        exitFlag = true;
                        System.out.println("You Fall Off The Maze. Game Over!");
                        break;
                    }

                    break;

                default:
                    exitFlag = true;
                    break;
            }
        }
        while (!exitFlag);

        //PRINTS THE EXIT COORDINATES
        if (lifeBank >= 0) {
            System.out.printf("Exit on (%d,%d)\n", entranceRow, entranceColumn);
        }
        //ADD EXIT COORDINATE TO ROUTE
        routeDesc = routeDesc.concat("(" + entranceRow + "," + entranceColumn + ")");

        //PRINTS THE FINAL ROUTE
        System.out.println(routeDesc);

        //PRINT THE FINAL MATRIX
        printMatrixDelta(pathMatrix, matrix);

        //CLOSING RESOURCES - IMPORTANT
        scanner.close();
    }

    private static int[] getMazeEntrance(char[][] matrix) {
        // FINAL VARIABLES
        final char DOWN_SIGN = 'V';
        final char UP_SIGN = '^';
        final char LEFT_SIGN = '<';
        final char RIGHT_SIGN = '>';

        int[] entranceCoordinate = new int[2]; //0=ROW,1=COLUMN
        int entranceRow = 0;
        int entranceColumn = 0;

        int longestRouteSteps = 0;
        int row;
        int column;

        //LOOP OVER FIRST ROW
        row = 0;
        for (column = 0; column < matrix[row].length; column++) {
            if (matrix[row][column] == DOWN_SIGN) {
                char[][] traceMatrix = findTraceMatrix(matrix, row, column);
                int stepCount = countSteps(traceMatrix);
                if (stepCount > longestRouteSteps) {
                    longestRouteSteps = stepCount;
                    entranceRow = row;
                    entranceColumn = column;
                }
            }
        }


        //LOOP OVER LAST ROW
        row = matrix.length - 1;
        for (column = 0; column < matrix[row].length; column++) {
            if (matrix[row][column] == UP_SIGN) {
                char[][] traceMatrix = findTraceMatrix(matrix, row, column);
                int stepCount = countSteps(traceMatrix);
                if (stepCount > longestRouteSteps) {
                    longestRouteSteps = stepCount;
                    entranceRow = row;
                    entranceColumn = column;
                }
            }
        }


        //LOOP OVER FIRST COLUMN
        column = 0;
        for (row = 0; row < matrix.length; row++) {
            if (matrix[row][column] == RIGHT_SIGN) {
                char[][] traceMatrix = findTraceMatrix(matrix, row, column);
                int stepCount = countSteps(traceMatrix);
                if (stepCount > longestRouteSteps) {
                    longestRouteSteps = stepCount;
                    entranceRow = row;
                    entranceColumn = column;
                }
            }
        }

        //LOOP OVER FIRST COLUMN
        column = matrix[0].length - 1;
        for (row = 0; row < matrix.length; row++) {
            if (matrix[row][column] == LEFT_SIGN) {
                char[][] traceMatrix = findTraceMatrix(matrix, row, column);
                int stepCount = countSteps(traceMatrix);
                if (stepCount > longestRouteSteps) {
                    longestRouteSteps = stepCount;
                    entranceRow = row;
                    entranceColumn = column;
                }
            }
        }

        //ASSIGNING THE COORDINATE TO THE ARRAY
        entranceCoordinate[0] = entranceRow;
        entranceCoordinate[1] = entranceColumn;

        //CHECK IF LONGEST ROUTE EXISTS
        if (longestRouteSteps == 0) {
            System.err.println("The Matrix you entered is not playable.");
        }

        return entranceCoordinate;
    }

    private static int countSteps(char[][] matrix) {
        final char BEEN_HERE_SIGN = '#';
        int stepCounter = 0;
        for (char[] chars : matrix) {
            for (int column = 0; column < chars.length; column++) {
                if (chars[column] == BEEN_HERE_SIGN) {
                    stepCounter++;
                }
            }
        }
        return stepCounter;
    }

    private static void printMatrixDelta(char[][] pathMatrix, char[][] matrix) {
        //FINAL COLOR VARIABLES
        final String ANSI_RED = "\u001B[31m";
        final String ANSI_RESET = "\u001B[0m";

        //PRINT OUT A MATRIX - WITH CONSIDERATION TO THE DELTA
        System.out.println();
        for (int row = 0; row < matrix.length; row++) {
            System.out.print("|");
            for (int column = 0; column < matrix[0].length; column++) {

                if (matrix[row][column] != pathMatrix[row][column]) {
                    //DELTA
                    System.out.print(ANSI_RED + pathMatrix[row][column] + ANSI_RESET);
                } else {
                    //NO DELTA
                    System.out.print(pathMatrix[row][column]);
                }
                if (column < matrix[0].length - 1) {
                    //SPACING
                    System.out.print(",");
                }
            }
            System.out.println("|");
        }
    }

    private static char inputDecision(Scanner scanner) {
        // FINAL VARIABLES
        final char DOWN_SIGN = 'V';
        final char UP_SIGN = '^';
        final char LEFT_SIGN = '<';
        final char RIGHT_SIGN = '>';

        // GET INPUT OF ROWS FROM END USER
        char decision;

        do {
            System.out.println("Will you take a chance to guess a direction? n=NO or <,>,V,^");
            decision = scanner.next().charAt(0);
        }
        while ((decision != 'n') && (decision != DOWN_SIGN) && (decision != UP_SIGN) && (decision != LEFT_SIGN) && (decision != RIGHT_SIGN));

        return decision;
    }

    private static char[][] copyMatrix(char[][] matrix) {
        char[][] copy = new char[matrix.length][matrix[0].length];

        for (int row = 0; row < copy.length; row++) {
            for (int column = 0; column < copy[0].length; column++) {
                char content = matrix[row][column];
                copy[row][column] = content;
            }
        }

        return copy;
    }

    private static boolean isWalkable(char cellValue) {
        // FINAL VARIABLES
        final char BEEN_HERE_SIGN = '#';
        final char GRASS_SIGN = 'O';
        final char MINE_SIGN = '*';

        //CHECK WHETHER YOU CAN PASS TO THE NEXT CELL OR NOT
        return (cellValue != GRASS_SIGN) && (cellValue != MINE_SIGN) && (cellValue != BEEN_HERE_SIGN);
    }
}
