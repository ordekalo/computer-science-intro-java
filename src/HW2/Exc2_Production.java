package HW2;

import java.util.Scanner;

public class Exc2_Production {
    /**
     * Author: Or Dekalo
     * ID: 313138141
     * Exercise: 2
     * MURBLE PUZZLE GAME
     */
    public static void main(String[] args) {
        //JUST EXECUTE AND ENJOY:)
        playMurblePuzzle();
    }

    private static void solveMurblePuzzle(char[] murblePuzzle) {
        int emptyPosition = murblePuzzle.length - 1;

        //START CONDITION
        printMarblePuzzle(murblePuzzle);

        //REPEATING PATTERN OF THE MURBLE
        shiftRight(murblePuzzle, emptyPosition - 1);
        emptyPosition--;

        do {

            while (!isBeforeExact(murblePuzzle, emptyPosition) && jumpRight(murblePuzzle, emptyPosition - 2)) {
                emptyPosition -= 2;
            }

            while (murblePuzzle[emptyPosition + 1] == murblePuzzle[emptyPosition - 1]) {
                shiftLeft(murblePuzzle, emptyPosition + 1);
                emptyPosition++;
            }

            while (!isAfterExact(murblePuzzle, emptyPosition) && jumpLeft(murblePuzzle, emptyPosition + 2)) {
                emptyPosition += 2;
            }
            while (murblePuzzle[emptyPosition - 1] == murblePuzzle[emptyPosition + 1]) {
                shiftRight(murblePuzzle, emptyPosition - 1);
                emptyPosition--;
            }

        } while (!validateOrder(murblePuzzle));
    }

    private static boolean isBeforeExact(char[] murblePuzzle, int position) {
        //CONSECUTIVE NUMBERS COUNTER
        int correctCounter = 0;

        //COUNT CONSECUTIVE NUMBERS
        char currentCell = murblePuzzle[0];
        for (int column = 0; column < position; column++) {
            if (currentCell == murblePuzzle[column]) {
                correctCounter++;
            }
        }

        //RETURN BOOLEAN - IF CONSECUTIVE NUMBERS IN FIRST SIZE
        return correctCounter == position;
    }

    private static boolean isAfterExact(char[] murblePuzzle, int position) {
        //CONSECUTIVE NUMBERS COUNTER
        int correctCounter = 0;

        //COUNT CONSECUTIVE NUMBERS
        char currentCell = murblePuzzle[position + 1];
        for (int column = position + 2; column < murblePuzzle.length; column++) {
            if (currentCell == murblePuzzle[column]) {
                correctCounter++;
            }
        }

        //RETURN BOOLEAN - IF CONSECUTIVE NUMBERS IN FIRST SIZE
        return correctCounter == (murblePuzzle.length - position - 2);
    }

    private static void shiftRight(char[] murblePuzzle, int column) {
        //FINAL VARIABLE
        final char BACKSPACE = ' ';

        //PREVENT NULL POINTER EXCEPTION
        if ((0 <= column) && (column + 1 <= murblePuzzle.length - 1) && (murblePuzzle[column + 1] == BACKSPACE)) {
            murblePuzzle[column + 1] = murblePuzzle[column];
            murblePuzzle[column] = BACKSPACE;

            //PRINT LAST ACTION TO CONSOLE
            System.out.print("  SR\n");

            //PRINT CONDITION TO CONSOLE
            printMarblePuzzle(murblePuzzle);

        }
    }

    private static void shiftLeft(char[] murblePuzzle, int column) {
        //FINAL VARIABLE
        final char BACKSPACE = ' ';

        //PREVENT NULL POINTER EXCEPTION
        if ((column <= murblePuzzle.length - 1) && (column != 0) && (murblePuzzle[column] == murblePuzzle[column - 2]) && (murblePuzzle[column - 1] == BACKSPACE)) {
            murblePuzzle[column - 1] = murblePuzzle[column];
            murblePuzzle[column] = BACKSPACE;

            //PRINT LAST ACTION TO CONSOLE
            System.out.print("  SL\n");

            //PRINT CONDITION TO CONSOLE
            printMarblePuzzle(murblePuzzle);

        }

    }

    private static boolean jumpRight(char[] murblePuzzle, int column) {
        //FINAL VARIABLE
        final char BACKSPACE = ' ';

        //PREVENT NULL POINTER EXCEPTION
        if ((1 <= column) && (column + 2 <= murblePuzzle.length - 1) && (murblePuzzle[column + 2] == BACKSPACE)) {
            murblePuzzle[column + 2] = murblePuzzle[column];
            murblePuzzle[column] = BACKSPACE;

            //PRINT LAST ACTION TO CONSOLE
            System.out.print("  JR\n");

            //PRINT CONDITION TO CONSOLE
            printMarblePuzzle(murblePuzzle);

            return true;
        }
        //IF REACHED HERE THEN IF DIDN'T EXECUTE
        return false;
    }

    private static boolean jumpLeft(char[] murblePuzzle, int column) {
        //FINAL VARIABLE
        final char BACKSPACE = ' ';

        //PREVENT NULL POINTER EXCEPTION
        if ((column + 2 <= murblePuzzle.length - 1) && (column - 2 >= 0) && (murblePuzzle[column - 2] == BACKSPACE)) {
            murblePuzzle[column - 2] = murblePuzzle[column];
            murblePuzzle[column] = BACKSPACE;

            //PRINT LAST ACTION TO CONSOLE
            System.out.print("  JL\n");

            //PRINT CONDITION TO CONSOLE
            printMarblePuzzle(murblePuzzle);

            return true;
        }
        //IF REACHED HERE THEN IF DIDN'T EXECUTE
        return false;
    }

    private static boolean validateOrder(char[] murblePuzzle) {
        //FINAL VARIABLE
        final char BACKSPACE = ' ';

        //SIZE FOR THE CHECK
        int size = murblePuzzle.length / 2;

        //CHECK IF BACKSPACE IN THE MIDDLE OF MURBLE PUZZLE SOLUTION
        if (murblePuzzle[size] != BACKSPACE) {
            return false;
        }

        //CONSECUTIVE NUMBERS COUNTER
        int correctCounter = 0;

        //COUNT CONSECUTIVE NUMBERS
        char currentCell = murblePuzzle[0];
        for (int column = 0; column < size; column++) {
            if (currentCell == murblePuzzle[column]) {
                correctCounter++;
            }
        }

        //RETURN BOOLEAN - IF CONSECUTIVE NUMBERS IN FIRST SIZE
        return correctCounter == size;
    }

    private static void printMarblePuzzle(char[] murblePuzzle) {
        System.out.print("|");
        for (char currentCell : murblePuzzle) {
            System.out.print(currentCell + "|");
        }
    }

    private static char[] createMurbleArray(int size) {
        //MATH LOGIC OF ARRAY LENGTH
        int length = 1 + size * 2;

        //CREATE MURBLE PUZZLE ARRAY
        char[] murblePuzzle = new char[length];

        //LOOP OVER THE EMPTY ARRAY
        for (int column = 0; column < murblePuzzle.length; column++) {

            if (column == murblePuzzle.length - 1) {
                //CHECK IF LAST -> LAST=BACKSPACE
                murblePuzzle[column] = ' ';
            } else if (column % 2 == 0) {
                //IF EVEN THAN EQUAL '0'
                murblePuzzle[column] = '0';
            } else {
                //IF ODD THAN EQUAL 'X'
                murblePuzzle[column] = 'X';
            }
        }

        //RETURN THE FILLED ARRAY
        return murblePuzzle;
    }

    private static int inputSize() {
        //CREATE SCANNER OBJECT FOR USER INPUTS
        Scanner scanner = new Scanner(System.in);

        int size;

        //INPUT VALIDATION
        do {
            System.out.println("Please Enter A Size Between 2 to 20:");
            size = scanner.nextInt();
        }
        while (size < 2 || size > 20);

        //CLOSING RESOURCES - IMPORTANT
        scanner.close();

        return size;
    }

    private static void playMurblePuzzle() {

        //SIZE INPUT FROM USER
        int size = inputSize();

        //CREATING THE MURBLE PUZZLE ARRAY
        char[] murblePuzzle = createMurbleArray(size);

        //SOLVING THE MURBLE PUZZLE
        solveMurblePuzzle(murblePuzzle);

    }
}
