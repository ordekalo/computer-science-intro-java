package lesson3;

import java.util.Scanner;

public class Exc1 {
    public static void main(String[] args) {
        int rooms;
        int isDuplex;
        int debt = 180;

        System.out.println("How many rooms?");
        Scanner scanner = new Scanner(System.in);
        rooms = scanner.nextInt();
        System.out.println("Is duplex?(1-True,0-False)");
        isDuplex = scanner.nextInt();
        scanner.close();
        debt += ((rooms == 3) ? 120 : 150) + ((isDuplex == 1) ? 20 : 0);
        System.out.printf("Your Debt is %d", debt);
    }
}
