package Lesson11;

public class Employee {
 private int id;
 private String name;
 private Section section;
 private static int minExperience;
 private static int serial = 1000;
 
 public enum Section {
		QA,
		RnD,
		Finance
	}
 
public int getId() {
	return id;
}


public Employee(String name, Section section) {
	super();
	this.id = serial++;
	this.name = name;
	this.section = section;
}


public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Section getSection() {
	return section;
}
public void setSection(Section section) {
	this.section = section;
}
public static int getMinExperience() {
	return minExperience;
}
public static void setMinExperience(int minExperience) {
	Employee.minExperience = minExperience;
}

@Override
public String toString() {
	return "Employee [id=" + id + ", name=" + name + ", section=" + section + "]";
}
public static void main(String[] args) {
	Employee employee = new Employee("Employee",Section.Finance);
	
	System.out.println(employee);
}
}
