package Lesson11;

public class Clock {
	private int hours, minutes;
	private final int maxHours = 24, maxMinutes = 60;
	private Format format;

	public enum Format {
		ampm, day
	}

	public enum Result {
		succeed, failed
	}

	public int getHours() {
		return hours;
	}

	public Result setHours(int hours) {
		if (hours >= 0 && hours <= maxHours) {
			this.hours = hours;
			return Result.succeed;
		}
		return Result.failed;
	}

	public int getMinutes() {
		return minutes;
	}

	public Result setMinutes(int minutes) {
		if (minutes >= 0 && minutes <= this.maxMinutes) {
			this.minutes = minutes;
			return Result.succeed;
		}
		return Result.failed;
	}

	public Format getFormat() {
		return format;
	}

	public void setFormat(Format format) {
		this.format = format;
	}

	public Clock(int hours, int minutes, Format format) {
		this.hours = hours;
		this.minutes = minutes;
		this.format = format;
	}

	public Clock(Clock clock) {
		this.hours = clock.hours;
		this.minutes = clock.minutes;
		this.format = clock.format;
	}

	@Override
	public String toString() {
		if (format == Format.ampm) {
			String min = (minutes < 10) ? "0" + minutes : "" + minutes;
			String h = (hours < 10) ? "0" + hours : "" + hours;
			return "(" + h + ":" + min + ")";

		} else {
			int newHours = hours%12;
			String min = (minutes < 10) ? "0" + minutes : "" + minutes;
			String h = (newHours < 10) ? "0" + newHours : "" + newHours;
			return "(" + h + ":" + min + ")";

		}
	}

	public static void main(String[] args) {
		Clock clock = new Clock(9, 8, Format.ampm);
		
		System.out.println(clock);
	}

}
