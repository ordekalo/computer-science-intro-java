package Mathematics;

import java.util.Scanner;

public class Cosinus {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a radian between 0 to 35:");
        double radian = scanner.nextInt();
        while (radian < 0 || radian > 35) {
            System.out.println("Please enter a radian between 0 to 35");
            radian = scanner.nextDouble();
        }

        System.out.println("Please enter an approximation between 40 to 70");
        int approximation = scanner.nextInt();
        while (approximation < 40 || approximation > 70) {
            System.out.println("Please enter an approximation between 40 to 70");
            approximation = scanner.nextInt();
        }
        scanner.close();
        
        double cosX = calculateCosX(radian, approximation);

        System.out.printf("cos(%.1f) in approximation of %d is %.3f", radian, approximation, cosX);
        System.out.printf("\ncos(%.1f) in calculator is: %.3f", radian, Math.cos(radian));
    }

    private static double calculateCosX(double radian, double approximately) {
        double cosx = 0;
        for (int i = 0; i < approximately; i++) {
            int exponent = 2 * i;
            double calculation = Math.pow(-1, i);
            calculation *= Math.pow(radian, exponent);
            calculation /= calculateFactorial(exponent);
            cosx += calculation;
        }
        return cosx;
    }

    private static double calculateFactorial(int exponent) {
        double factorial = 1;
        for (int j = 1; j <= exponent; j++) {
            factorial *= j;
        }
        return factorial;
    }
}
