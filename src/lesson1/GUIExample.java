package lesson1;

import javax.swing.*;

public class GUIExample {
    public static void main(String[] args) {
        int age = Integer.parseInt(JOptionPane.showInputDialog("Please type your age"));
        age += 1;
        JOptionPane.showMessageDialog(null, "Next year you will be " + age
                + " years old");
    }
}
