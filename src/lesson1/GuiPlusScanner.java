package lesson1;

import javax.swing.*;
import java.util.Calendar;
import java.util.Scanner;

public class GuiPlusScanner {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = JOptionPane.showInputDialog("Please enter your name");
        int age = Integer.parseInt(JOptionPane.showInputDialog("Please enter your age"));
        System.out.println("Please enter any year");
        int year = scanner.nextInt();
        int year_now = Calendar.getInstance().get(Calendar.YEAR);
        int delta = year - year_now + age;
        JOptionPane.showMessageDialog(null, "Hi " + name +
                " you will be " + delta + " years old in year " + year);

    }
}
