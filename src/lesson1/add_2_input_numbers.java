package lesson1;

import java.util.Scanner;

public class add_2_input_numbers {
    public static void main(String[] args) {
        double num1;
        double num2;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please Provide First Number:");
        num1 = scanner.nextDouble();
        System.out.println("Please Provide Second Number:");
        num2 = scanner.nextDouble();

        System.out.println("The Result is : "+(num1 + num2));
    }


}
