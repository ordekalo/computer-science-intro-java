package lesson2;

import java.util.Scanner;

public class Exc4 {
    public static void main(String[] args) {
        System.out.println("Let's go !");
        float init_price = 10.2F;
        float km_price = 1.3F;
        float luggage_price = 2.0F;

        Scanner scanner = new Scanner(System.in);

        System.out.println("How many KM?");
        float price = scanner.nextFloat() * km_price + init_price;

        System.out.println("How many Luggage?");
        price += luggage_price * (float) scanner.nextInt();
        scanner.close();

        System.out.println("The ride price: " + price + " without VAT");


    }
}
