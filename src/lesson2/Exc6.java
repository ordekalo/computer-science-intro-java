package lesson2;

import java.util.Scanner;

public class Exc6 {
    public static void main(String[] args) {
        System.out.println("Please provide 4 digits number:");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int temp_a = 0;
        while (num > 0) {
            temp_a = num % 10 + temp_a * 10;
            num = num / 10;
        }

        System.out.println("Reversed Number is: " + temp_a);
    }
}
