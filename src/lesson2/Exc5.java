package lesson2;

import java.util.Scanner;

public class Exc5 {
    public static void main(String[] args) {
        System.out.println("Which floor do you want to go?");
        Scanner scanner = new Scanner(System.in);
        int floor = scanner.nextInt();
        scanner.close();
        int eta = (floor - 1) * 5 * 3;
        System.out.println("Estimate Time Arrival: " + eta + " seconds.");
    }
}
