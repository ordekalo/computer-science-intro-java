package lesson2;

import java.util.Calendar;
import java.util.Scanner;

public class Exc1 {

    public static void main(String[] args) {
        int birth_year, id, height;
        char gender;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please provide birth year:");
        birth_year = scanner.nextInt();
        System.out.println("Please provide your id:");
        id = scanner.nextInt();
        System.out.println("Please provide your height:");
        height = scanner.nextInt();
        System.out.println("Please provide your gender: M-MALE F-FEMALE");
        gender = scanner.next().charAt(0);

        int age = Calendar.getInstance().get(Calendar.YEAR) - birth_year;

        System.out.println("Age : " + age + " \\\\ ID : " + id);
        System.out.println("Height : " + height + " \\\\ Gender : " + gender);
        scanner.close();
    }
}
